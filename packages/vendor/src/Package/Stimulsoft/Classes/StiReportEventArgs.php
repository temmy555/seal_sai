<?php

namespace Vendor\Package\Stimulsoft\Classes;

class StiReportEventArgs
{
    public $sender = null;
    public $report = null;

    function __construct($sender, $report = null)
    {
        $this->sender = $sender;
        $this->report = $report;
    }
}
