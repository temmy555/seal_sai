<?php
Route::group(['middleware' => ['web', 'revalidate']], function () {
    Route::get('login', 'Admin\Auth\ALoginController@index')->name('login');
    Route::get('logout', 'Admin\Auth\ALoginController@logout')->name('logout');
    Route::post('login', 'Admin\Auth\ALoginController@authenticate');

    Route::group(['middleware' => ['auth', 'auth.check']], function () {
        Route::get('/', 'Admin\Dashboard\ADashboardController@index')->name('home');
        Route::group(['prefix' => 'tools'], function () {
            Route::get('user_cabang', 'Admin\Tools\Popup@getUserCabang')->name('tools.user_cabang');
            Route::post('user_cabang', 'Admin\Tools\Popup@postUserCabang')->name('tools.user_cabang');
        });
        Route::group(['prefix' => 'master'], function () {
            Route::group(['prefix' => 'muatan_luar'], function () {
                Route::get('del', 'Admin\Master\AMuatanLuarController@deleteData')->name('muatan_luar.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMuatanLuarController@indexDetail')->name('muatan_luar.detail');
                    Route::post('/', 'Admin\Master\AMuatanLuarController@postDetail')->name('muatan_luar.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMuatanLuarController@indexList')->name('muatan_luar.list');
                });
            });
            Route::group(['prefix' => 'muatan_dalam'], function () {
                Route::get('del', 'Admin\Master\AMuatanDalamController@deleteData')->name('muatan_dalam.delete');
                Route::get('app', 'Admin\Master\AMuatanDalamController@approve');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AMuatanDalamController@indexDetail')->name('muatan_dalam.detail');
                    Route::post('/', 'Admin\Master\AMuatanDalamController@postDetail')->name('muatan_dalam.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AMuatanDalamController@indexList')->name('muatan_dalam.list');
                });
            });
            Route::group(['prefix' => 'biaya_umum'], function () {
                Route::get('del', 'Admin\Master\ABiayaUmumController@deleteData')->name('biaya_umum.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\ABiayaUmumController@indexDetail')->name('biaya_umum.detail');
                    Route::post('/', 'Admin\Master\ABiayaUmumController@postDetail')->name('biaya_umum.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\ABiayaUmumController@indexList')->name('biaya_umum.list');
                });
            });
            Route::group(['prefix' => 'biaya_kir'], function () {
                Route::get('del', 'Admin\Master\ABiayaKirController@deleteData')->name('biaya_kir.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\ABiayaKirController@indexDetail')->name('biaya_kir.detail');
                    Route::post('/', 'Admin\Master\ABiayaKirController@postDetail')->name('biaya_kir.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\ABiayaKirController@indexList')->name('biaya_kir.list');
                });
            });
            Route::group(['prefix' => 'truck'], function () {
                Route::get('del', 'Admin\Master\ATruckController@deleteData')->name('truck.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\ATruckController@indexDetail')->name('truck.detail');
                    Route::post('/', 'Admin\Master\ATruckController@postDetail')->name('truck.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\ATruckController@indexList')->name('truck.list');
                });
            });
            Route::group(['prefix' => 'equipment_expenses'], function () {
                Route::get('del', 'Admin\Master\AEquipmentController@deleteData')->name('equipment_expenses.delete');
                Route::get('detail', 'Admin\Master\AEquipmentController@indexDetail')->name('equipment_expenses.detail');
                Route::post('detail_log', 'Admin\Master\AEquipmentController@postDetail')->name('equipment_expenses.log');
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AEquipmentController@indexList')->name('equipment_expenses.list');
                    Route::post('/', 'Admin\Master\AEquipmentController@postModel')->name('model.post');
                });
            });
            Route::group(['prefix' => 'uang_kerja'], function () {
                Route::get('del', 'Admin\Sistem\ASistemUserController@deleteData')->name('uang_kerja.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AUangKerjaController@indexDetail')->name('uang_kerja.detail');
                    Route::post('/', 'Admin\Master\AUangKerjaController@postDetail')->name('uang_kerja.post');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AUangKerjaController@indexList')->name('uang_kerja.list');
                });
            });
            Route::group(['prefix' => 'biaya_staff'], function () {
                Route::get('del', 'Admin\Master\ABiayaStaffController@deleteData')->name('biaya_staff.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\ABiayaStaffController@indexDetail')->name('biaya_staff.detail');
                    Route::post('/', 'Admin\Master\ABiayaStaffController@postDetail')->name('biaya_staff.post');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\ABiayaStaffController@indexList')->name('biaya_staff.list');
                });
            });
        });
        Route::group(['prefix' => 'olap'], function () {
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexDetail')->name('olap.detail');
                Route::post('/', 'Admin\OLAP\AOLAPController@postDetail')->name('olap.detail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexList')->name('olap.list');
            });
            Route::group(['prefix' => 'print_out'], function () {
                Route::get('data', 'Admin\OLAP\APrintOutController@getData')->name('olap.print_out.data');
                Route::group(['prefix' => 'view'], function () {
                    Route::get('/', 'Admin\OLAP\APrintOutController@index')->name('olap.print_out.view');
                    Route::post('/', 'Admin\OLAP\APrintOutController@index');
                });
            });
        });
        Route::group(['prefix' => 'invoice'], function () {
            Route::group(['prefix' => 'data_trip'], function () {
                Route::get('del', 'Admin\Invoice\ADataTripController@deleteData')->name('data_trip.delete');
                Route::get('/', 'Admin\Invoice\ADataTripController@indexDetail')->name('data_trip.detail');
                Route::post('/', 'Admin\Invoice\ADataTripController@postDetail')->name('data_trip.detail');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Invoice\ADataTripController@indexDetail')->name('data_trip.detail');
                    Route::post('/', 'Admin\Invoice\ADataTripController@postDetail')->name('data_trip.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Invoice\ADataTripController@indexList')->name('data_trip.list');
                });
            });
            Route::group(['prefix' => 'kwitansi'], function () {
                Route::get('del', 'Admin\Invoice\ADataController@deleteData')->name('kwitansi.delete');
                Route::get('add', 'Admin\Invoice\ADataController@postInvoice')->name('kwitansi.add');
                Route::get('audit', 'Admin\Invoice\ADataController@audit')->name('kwitansi.audit');
                Route::get('pembayaran', 'Admin\Invoice\ADataController@pembayaran')->name('kwitansi.pembayaran');
                Route::post('pelunasan', 'Admin\Invoice\ADataController@postPembayaran')->name('kwitansi.pelunasan');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Invoice\ADataController@indexDetail')->name('kwitansi.detail');
                    Route::post('/', 'Admin\Invoice\ADataController@postDetail')->name('kwitansi.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Invoice\ADataController@indexList')->name('kwitansi.list');
                });
            });
            Route::group(['prefix' => 'approve'], function () {
                Route::get('del', 'Admin\Invoice\AApproveController@deleteData')->name('approve.delete');
                Route::get('app', 'Admin\Invoice\AApproveController@approve')->name('approve.app');
                Route::get('deskripsi', 'Admin\Invoice\AApproveController@deskripsi')->name('approve.deskripsi');
                Route::post('post', 'Admin\Invoice\AApproveController@post_deskripsi')->name('deskripsi.post');
                Route::get('print', 'Admin\Invoice\AApproveController@print')->name('approve.print');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Invoice\AApproveController@indexDetail')->name('approve.detail');
                    Route::post('/', 'Admin\Invoice\AApproveController@postDetail')->name('approve.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Invoice\AApproveController@indexList')->name('approve.list');
                });
            });
        });
        Route::group(['prefix' => 'report'], function () {
            Route::group(['prefix' => 'today-all'], function () {
                Route::get('/', 'Admin\Report\AReportController@index')->name('report.today-all');
            });
            Route::group(['prefix' => 'all'], function () {
                Route::get('/', 'Admin\Report\AReportController@index2')->name('report.all');
            });
        });
        Route::group(['prefix' => 'olap'], function () {
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexDetail')->name('olap.detail');
                Route::post('/', 'Admin\OLAP\AOLAPController@postDetail')->name('olap.detail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexList')->name('olap.list');
            });
            Route::group(['prefix' => 'print_out'], function () {
                Route::get('data', 'Admin\OLAP\APrintOutController@getData')->name('olap.print_out.data');
                Route::group(['prefix' => 'view'], function () {
                    Route::get('/', 'Admin\OLAP\APrintOutController@index')->name('olap.print_out.view');
                    Route::post('/', 'Admin\OLAP\APrintOutController@index');
                });
            });
        });
        Route::group(['prefix' => 'sistem'], function () {
            Route::group(['prefix' => 'user'], function () {
                Route::get('del', 'Admin\Sistem\ASistemUserController@deleteData')->name('user.delete');
                Route::get('ganti-password', 'Admin\Sistem\ASistemUserController@ganti_password')->name('password.reset');
                Route::post('update', 'Admin\Sistem\ASistemUserController@updatePassword')->name('password.update');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemUserController@indexDetail')->name('user.detail');
                    Route::post('/', 'Admin\Sistem\ASistemUserController@postDetail')->name('user.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemUserController@indexList')->name('user.list');
                });
            });
            Route::group(['prefix' => 'role_user'], function () {
                Route::get('s2', 'Admin\Sistem\ASistemRoleUserController@searchData')->name('s2.role_user');
                Route::get('del', 'Admin\Sistem\ASistemRoleUserController@deleteData')->name('role_user.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexDetail')->name('role_user.detail');
                    Route::post('/', 'Admin\Sistem\ASistemRoleUserController@postDetail')->name('role_user.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexList')->name('role_user.list');
                });
            });
        });

    });
});
