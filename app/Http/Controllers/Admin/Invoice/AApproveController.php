<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\DataInvoiceHeader;
use App\Models\DataInvoicePrint;
use App\Models\DataMuatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class AApproveController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataInvoicePrint::with(['users'])->find($uid);
            return view('admin.menus.master.data_trip.detail', compact('detail'));
        }
        return view('admin.menus.invoice.list_invoice.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.invoice.approve.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            $uid = $inputs->input('id');
            $message = 'Data Muatan Dalam Berhasil Dibuat';

            $data = new DataInvoicePrint();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataInvoicePrint::find($uid);
                $message = 'Data Muatan Dalam Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'MUATAN_DALAM';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $data->penerima = $inputs->input('penerima');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->muatan = $inputs->input('muatan');
            $data->no_pol = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->nilai_awal = $inputs->input('nilai_awal');
            $data->users_id = auth()->id();
            $data->save();
            return Helper::redirect('data.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataInvoicePrint::find($uid)->delete();

        return Helper::redirect('approve.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function approve(Request $request)
    {
        $message = 'Invoice Berhasil di Approve';
        $uid = $request->input('id');
//        return $uid;
        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DB::table('data_invoice_print')
            ->where('data_invoice_header_id', $uid)
            ->update(['approve' => 1]);

        return Helper::redirect('approve.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
    public function deskripsi(Request $request)
    {
        $uid = $request->input('id');
        $data = DB::table('data_invoice_print')
            ->join('data_invoice_header','data_invoice_print.data_invoice_header_id','=','data_invoice_header.id')
            ->select('data_invoice_print.id','data_invoice_header.no_trip','data_invoice_header.tanggal','data_invoice_header.total_tagihan','data_invoice_print.deskripsi')
            ->where('data_invoice_print.data_invoice_header_id','=',$uid)
            ->get();

//        return $data;

        return view('admin.menus.invoice.approve.deskripsi',compact('data'));
    }
    public function post_deskripsi(Request $request)
    {
        $invoice = $request->input('invoice');
        foreach ($invoice as $i => $val) {
//            echo $i . "=" . $val['description'];
            DB::table('data_invoice_print')
                ->where('id', $i)
                ->update(['deskripsi' => $val['description']]);
        }

        $message = "SUKSES";
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
    public function getQuery(Request $request)
    {
        $query ='select dih.id, dih.no_invoice,dih.no_trip, dip.approve, dih.keterangan, sum(dm.jumlah) as jumlah, (dih.total_tagihan+dih.ppn) as tagihan, u.username from users u
                    join data_invoice_print dip on u.id = dip.users_id
                    join data_invoice_header dih on dip.data_invoice_header_id = dih.id
                    join data_invoice_detail did on dih.id = did.data_invoice_header_id
                    join data_muatan dm on did.data_muatan_id = dm.id
                    group by dih.id, dih.no_invoice, dih.no_trip, dih.kepada, dip.approve, dih.keterangan, u.username, tagihan
                    order by dih.id desc;';

        return DB::select(DB::raw($query));
    }

    public function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $action = "";
                if (Helper::checkAccess($request, 'PRINT')) {
                    if ($data->approve == 1) {
                        $action .= '<a href="' . route('olap.print_out.view', ['print' => 'Invoice', 'id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
                        <i class="la la-print"></i>
                    </a>';
                    }
                }
                return $action;
            })
            ->editColumn('approve', function ($data) use ($request) {
                $approve = "";
                if (Helper::checkAccess($request, 'APPROVE')) {
                    if ($data->approve == 0) {
                        $approve .= '<a href="' . url('invoice/approve/app?id=' . $data->id) . '" class="btn btn-sm btn-danger">NO
                    </a>';
                    } else {
                        $approve .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">APPROVED</span>';
                    }
                }
                return $approve;
            })
            ->editColumn('jumlah', function ($data) {
                return ( number_format( $data->jumlah,'0', '.', ','));
            })
            ->editColumn('tagihan', function ($data) {
                return ( number_format( $data->tagihan,'0', '.', ','));
            })
            ->editColumn('no_invoice', function ($data){
                return '<u><b><a href="' . url('invoice/approve/deskripsi?id='. $data->id) .'">' . $data->no_invoice .'</b></u></a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

//if (data.getData("ppn")>0) {
//Text20.enabled=true;
//Text21.enabled=true;
//Text16.enabled=true;
//Text26.setEnabled(true);
//Text27.setTop(0);
//}
}
