<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ADataTripController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.data_trip.detail', compact('detail'));
        }
        return view('admin.menus.invoice.data_trip.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.invoice.data_trip.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            //generate no_trip
            $q = DataMuatan::where('no_trip', 'like', 'D%')
                ->latest()
                ->first();

            $new_code = '';
            $last = '';
            if ($q == null) {
                $new_code = 'D1';
            } else {
                // $last = Str::substr($hasil,2,1);
                $hasil = $q->no_trip;
                $last = (int)(Str::substr($hasil, 1, 5)) + 1;
                $new_code = 'D' . $last;
            }
            $uid = $inputs->input('id');
            $message = 'Data Muatan Dalam Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Data Muatan Dalam Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'MUATAN_DALAM';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $data->penerima = $inputs->input('penerima');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->muatan = $inputs->input('muatan');
            $data->no_pol = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->nilai_awal = $inputs->input('nilai_awal');
            $data->users_id = auth()->id();
            $data->save();
            return Helper::redirect('data_trip.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('data_trip.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $model = DataMuatan::query()->with(['users'])->where('kode_data', 'MUATAN_LUAR')->where('flag', 0);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                if (Helper::checkAccess($request, 'DELETE')) {
                    $actions .= '<a href="javascript:void(0);" data-url="' . url('invoice/data_trip/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                }
                return $actions;
            })
            ->addColumn('select', function ($data) {
                return
                    '<input type="checkbox" name="check[]" class="form-control" value="'.$data->id.'">';
            })
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
