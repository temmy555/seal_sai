<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class ALoginController extends Controller
{
    public function index(Request $request)
    {
//        $user = new User();
//        $user->username = 'temmmy';
//        $user->nama = 'TEMMY KURNIAWAN';
//        $user->password = Hash::make('12345');
//        $user->role_id = 2;
//        $user->save();
        return view('admin.menus.auth.login');
    }

    public function authenticate(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            $users = User::with(['users_company'])->find(Auth::id());
            $users->active_company_id = 0;
            $users->save();

            return response()->json(['message' => 'OK']);
        }
        return response()->json(['input' => $request->all(), 'message' => 'username atau password salah mohon dicek kembali!'], 503);
    }

//    public function authenticate(Request $request)
//    {
//        $username = $request->input('username');
//        $password = $request->input('password');
//        if (Auth::attempt(['username' => $username, 'password' => $password])) {
//            return redirect('/');
//        }
//        $error = [
//            'messages' => ['username atau password salah mohon dicek kembali!'],
//        ];
//        return redirect(route('login'))->withInput()->withErrors($error);
//    }

    public function logout()
    {
//        $users = User::with(['users_company'])->find(Auth::id());
//        $users->active_company_id = 0;
//        $users->save();
        Auth::logout();
        return redirect(route('login'));
    }

}
