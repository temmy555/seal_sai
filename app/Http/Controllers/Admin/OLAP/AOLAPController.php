<?php

namespace App\Http\Controllers\Admin\OLAP;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AOLAPController extends Controller
{
    public function indexDetail(Request $request)
    {
        return view('admin.menus.olap.detail');
    }

    public function postDetail(Request $request)
    {
        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $tipe = $inputs->input('tipe');
            $query = $inputs->input('query');
            $result = DB::connection($tipe)->select(DB::raw($query));
            $result = collect($result);
            if ($result->isNotEmpty()) {
                $column = collect($result[0])->toArray();
                $column = array_keys($column);
//                return view('admin.menus.olap.preview_devextreme', compact('column', 'result'));
                return view('admin.menus.olap.preview', compact('column', 'result'));
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }
}
