<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AUangKerjaController extends Controller
{
    public function indexDetail(Request $request)
    {
        return view('admin.menus.master.equipment.detail');
    }

    public function indexList(Request $request)
    {
        return view('admin.menus.master.uang_kerja.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('muatan_luar.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = DataMuatan::query()->with(['users'])->where('kode_data', 'UANG_KERJA')->where('tanggal',$today);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                if (Helper::checkAccess($request, 'DELETE')) {
                    $actions .= '<a href="javascript:void(0);" data-url="' . url('master/biaya_staff/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                }
                return $actions;
            })
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
