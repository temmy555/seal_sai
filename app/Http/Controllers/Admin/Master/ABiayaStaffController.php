<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;
use App\Models\DataMuatan;

class ABiayaStaffController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.biaya_staff.detail', compact('detail'));
        }
        return view('admin.menus.master.biaya_staff.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.biaya_staff.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('biaya_staff.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $model = DataMuatan::query()->where('kode_data','BIAYA_STAFF');
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) {
                $actions = "";
                    $actions .= '<a href="javascript:void(0);" data-url="' . url('master/biaya_staff/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                return $actions;
            })
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Biaya Staff Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Biaya Staff Berhasil Diedit';
            }
            $data->kode_data = 'BIAYA_STAFF';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->jumlah = $inputs->input('jumlah');
            $data->keterangan = $inputs->input('keterangan');
            $data->save();
            return Helper::redirect('biaya_staff.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    
}
