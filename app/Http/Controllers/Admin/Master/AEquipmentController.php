<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Models\EquipmentDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AEquipmentController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
//        return $uid;
        $equipment = Equipment::where('id', $uid)->get();
        $data = EquipmentDetail::with(['users'])->where('equipment_header_id', $uid)->orderBy('tanggal','desc')->get();

        return view('admin.menus.master.equipment.detail', compact('equipment', 'data'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            return $this->dataTable($request);
        }
        return view('admin.menus.master.equipment.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postModel(Request $request)
    {
        $inputs = Helper::merge($request);
        try {
            $message = 'Model Serie Berhasil Ditambahkan';
            $data = new Equipment();
            $data->model_series = $inputs->input('model_series');
            $data->status = 0;
            $data->save();

            return Helper::redirect('equipment_expenses.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }
    public function postDetail(Request $request)
    {
        $detail = new EquipmentDetail();
        $header_id = $request->input('header_id');
        $detail->equipment_header_id = $header_id;
        $detail->tanggal = $request->input('tanggal');
        $detail->jenis_biaya = $request->input('jenis_biaya');
        $detail->jumlah = $request->input('jumlah');
        $detail->supplier = $request->input('supplier');
        $detail->keterangan = $request->input('keterangan');
        $detail->users_id = auth()->id();
        $detail->save();

        $query2 = "select ed.jenis_biaya as status, ed.tanggal as tanggal from equipment e
                    join equipment_detail ed on e.id = ed.equipment_header_id
                    join users u on ed.users_id = u.id
                    where e.id = $header_id
                    order by ed.tanggal desc limit 1;";
        $data = Helper::selectMany($query2);
        $status = $data[0]->status;
        $tanggal = $data[0]->tanggal;

        DB::table('equipment')
            ->join('equipment_detail','equipment_detail.equipment_header_id','=','equipment.id')
            ->where('equipment.id', $header_id)
            ->update(['equipment.perawatan' => $status, 'equipment.tanggal' => $tanggal]);

        $message = "Detail Perawatan Tersimpan";

        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Equipment::find($uid)->delete();

        return Helper::redirect('equipment_expenses.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
    public function dataTable(Request $request)
    {
        $model = Equipment::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data){
                $actions = "";
                    $actions .= '<a href="javascript:void(0);" data-url="' . url('master/equipment/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                return $actions;
            })
            ->editColumn('model_series', function ($data) {
                return '<u><b><a href="' . url('master/equipment_expenses/detail?id=' . $data->id) . '">' . $data->model_series . '</b></u></a>';
            })
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->editColumn('perawatan', function ($data){
                $perawatan = "";
                if($data->perawatan == null){
                    $perawatan .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger kt-badge--rounded">BELUM ADA RIWAYAT</span>';
                }else{
                    $perawatan .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">' . $data->perawatan . '</span>';
                }
                return $perawatan;
            })
            ->editColumn('status', function ($data){
                $status = "";
                if($data->status == 0){
                    $status .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">SIAP DIPAKAI</span>';
                }else{
                    $status .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger kt-badge--rounded">DISEWAKAN</span>';
                }
                return $status;
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
