<?php

namespace App\Http\Middleware;

use App\Models\UsersCompany;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticationCheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            if ($request->routeIs('tools.user_cabang')) {
                return $next($request);
            }
            if (!Auth::user()->active_company) {
                return redirect(route('logout'));
            }
            if (!UsersCompany::where('users_id', Auth::id())->where('company_id', Auth::user()->active_company->id)->first()) {
                return redirect(route('logout'));
            }
        }
        return $next($request);
    }
}
