<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DataInvoiceDetail
 * 
 * @property int $id
 * @property int $data_invoice_header_id
 * @property int $data_muatan_id
 * 
 * @property DataInvoiceHeader $data_invoice_header
 * @property DataMuatan $data_muatan
 *
 * @package App\Models
 */
class DataInvoiceDetail extends Model
{
	protected $table = 'data_invoice_detail';
	public $timestamps = false;

	protected $casts = [
		'data_invoice_header_id' => 'int',
		'data_muatan_id' => 'int'
	];

	protected $fillable = [
		'data_invoice_header_id',
		'data_muatan_id'
	];

	public function data_invoice_header()
	{
		return $this->belongsTo(DataInvoiceHeader::class);
	}

	public function data_muatan()
	{
		return $this->belongsTo(DataMuatan::class);
	}
}
