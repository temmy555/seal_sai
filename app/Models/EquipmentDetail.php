<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EquipmentDetail
 *
 * @property int $id
 * @property int $equipment_header_id
 * @property Carbon $tanggal
 * @property string $jenis_biaya
 * @property int $jumlah
 * @property string $supplier
 * @property string $keterangan
 * @property int $users_id
 *
 * @property Equipment $equipment
 * @property User $user
 *
 * @package App\Models
 */
class EquipmentDetail extends Model
{
	protected $table = 'equipment_detail';
	public $timestamps = false;

	protected $casts = [
		'equipment_header_id' => 'int',
		'jumlah' => 'int',
		'users_id' => 'int'
	];

	protected $dates = [
		'tanggal'
	];

	protected $fillable = [
		'equipment_header_id',
		'tanggal',
		'jenis_biaya',
		'jumlah',
		'supplier',
		'keterangan',
		'users_id'
	];

	public function equipment()
	{
		return $this->belongsTo(Equipment::class, 'equipment_header_id');
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
