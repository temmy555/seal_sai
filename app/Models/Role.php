<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Role
 *
 * @property int $id
 * @property string $nama_role
 * @property string $keterangan
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|Modul[] $moduls
 *
 * @package App\Models
 */
class Role extends Model
{
    use SoftDeletes;
    protected $table = 'roles';

    protected $fillable = [
        'nama_role',
        'keterangan'
    ];

    public function moduls()
    {
        return $this->belongsToMany(Modul::class, 'roles_modul', 'roles_id', 'modul_id')
            ->where('modul.is_hidden', false)
            ->withPivot('id', 'parent_id', 'is_hidden')
            ->orderBy('parent_id', 'desc')
            ->orderBy('modul_id', 'asc');
    }

    public function roles_moduls()
    {
        return $this->hasMany(\App\Models\RolesModul::class, 'roles_id');
    }

    public function moduls_access()
    {
        return $this->belongsToMany(ModulAccess::class, 'roles_modul_access', 'roles_id', 'modul_access_id')
            ->withPivot('id');
    }

    public function roles_moduls_access()
    {
        return $this->hasMany(\App\Models\RolesModulAccess::class, 'roles_id');
    }
}
