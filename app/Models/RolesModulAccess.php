<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RolesModulAccess
 * 
 * @property int $id
 * @property int $roles_id
 * @property int $modul_access_id
 * 
 * @property ModulAccess $modul_access
 * @property Role $role
 *
 * @package App\Models
 */
class RolesModulAccess extends Model
{
	protected $table = 'roles_modul_access';
	public $timestamps = false;

	protected $casts = [
		'roles_id' => 'int',
		'modul_access_id' => 'int'
	];

	protected $fillable = [
		'roles_id',
		'modul_access_id'
	];

	public function modul_access()
	{
		return $this->belongsTo(ModulAccess::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'roles_id');
	}
}
