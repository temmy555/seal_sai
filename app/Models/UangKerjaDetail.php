<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UangKerjaDetail
 *
 * @property int $id
 * @property int $uang_kerja_header_id
 * @property int $penerima
 * @property int $jumlah
 * @property int $keterangan
 *
 * @property UangKerja $uang_kerja
 *
 * @package App\Models
 */
class UangKerjaDetail extends Model
{
	protected $table = 'uang_kerja_detail';
	public $timestamps = false;

	protected $casts = [
		'uang_kerja_header_id' => 'int',
		'penerima' => 'int',
		'jumlah' => 'int',
		'keterangan' => 'int'
	];

	protected $fillable = [
		'uang_kerja_header_id',
		'penerima',
		'jumlah',
		'keterangan'
	];

	public function uang_kerja()
	{
		return $this->belongsTo(UangKerja::class, 'uang_kerja_header_id');
	}
}
