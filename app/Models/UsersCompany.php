<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersCompany
 *
 * @property int $id
 * @property int $company_id
 * @property int $users_id
 *
 * @property Company $company
 * @property User $user
 *
 * @package App\Models
 */
class UsersCompany extends Model
{
	protected $table = 'users_company';
	public $timestamps = false;

	protected $casts = [
		'company_id' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'company_id',
		'users_id'
	];

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
