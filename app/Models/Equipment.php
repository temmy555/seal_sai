<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equipment
 * 
 * @property int $id
 * @property string $model_series
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Collection|EquipmentDetail[] $equipment_details
 * @property Collection|UangKerja[] $uang_kerjas
 *
 * @package App\Models
 */
class Equipment extends Model
{
	use SoftDeletes;
	protected $table = 'equipment';

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'model_series',
		'status'
	];

	public function equipment_details()
	{
		return $this->hasMany(EquipmentDetail::class, 'equipment_header_id');
	}

	public function uang_kerjas()
	{
		return $this->hasMany(UangKerja::class, 'equipment_header_id');
	}
}
