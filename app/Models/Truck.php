<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Role
 *
 * @property int $id
 * @property string $no_pol
 * @property string $jenis
 * @property string $type
 * @property string $keterangan
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Truck extends Model
{
    use SoftDeletes;
    protected $table = 'truck';

    protected $fillable = [
        'no_pol',
        'jenis',
        'type',
        'keterangan'
    ];
}
