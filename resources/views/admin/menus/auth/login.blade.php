<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Sarinah Group| Login </title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    {{--    <link href="{{ asset('assets/media/logos/favicon.ico') }}" rel="shortcut icon"/>--}}
    <style>
        .landing-logo {
            width: 415px;
            height: 100px;
        }

        .landing-bg {
            background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(255, 255, 255, 0.2)), url('{{asset('images/cover.jpg')}}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper {
            padding: 15% 2rem 1rem 2rem;
            margin: 0 auto 2rem auto;
            overflow: hidden;
        }

        .kt-login__signin__shadow {
            padding: 20px;
            border-radius: 25px;
            background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5));
        }

        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-form .form-control {
            height: 46px;
            border-radius: 46px;
            border: none;
            padding-left: 1.5rem;
            padding-right: 1.5rem;
            margin-top: 1.5rem;
            background: rgba(0, 0, 0, 0.4);
            color: #fff;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-login__head .kt-login__title {
            text-align: center;
            font-size: 2.2rem;
            font-weight: 500;
            color: #fff;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-form {
            margin: 2rem auto;
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor landing-bg">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__signin kt-login__signin__shadow">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Sarinah Group</h3>
                        </div>
                        <form id="form" class="kt-form" method="post" action="{{url()->current()}}">
                            @csrf
                            @if ($errors->any())
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text">
                                        <ul>
                                            @foreach ($errors->get('messages') as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Username" name="username"
                                       autocomplete="off">
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="Password" name="password">
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_signin_submit" type="submit"
                                        class="btn btn-lg btn-info">Sign In
                                </button>
                                <br><br>
                                <div class="kt-footer__copyright">
                                    <h6><font color="white">&nbsp;&copy;&nbsp;2020, Powered by <a href="{{url('/')}}"
                                                                                                  target="_blank"
                                                                                                  class="kt-link"><font
                                                    color="yellow">ITSOS</font></a></font></h6>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.includes.cabang')
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="{{ asset('js/app.js') }}"></script>
@stack('script')
<script>
    $('#form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "{{route('login')}}",
            data: $("#form").serialize(),
            success: function ($res) {
                onChangeCabangClick();
            },
            error: function ($err) {
                $err = $.parseJSON($err.responseText);
                $errMsg = "<div class=\"alert alert-danger fade show\" role=\"alert\">\n" +
                    "                                    <div class=\"alert-text\">\n" +
                    "                                        <ul>\n" +
                    "                                            <li>" + $err.message + "</li>\n" +
                    "                                        </ul>\n" +
                    "                                    </div>\n" +
                    "                                    <div class=\"alert-close\">\n" +
                    "                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
                    "                                            <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>\n" +
                    "                                        </button>\n" +
                    "                                    </div>\n" +
                    "                                </div>";
                $('#form').prepend($errMsg);
            }
        });
    });
</script>
</body>
</html>
