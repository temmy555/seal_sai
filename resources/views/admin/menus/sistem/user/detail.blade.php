@extends('admin.layouts.default')
@section('title', $title='Sistem User')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form id="form" class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Nama:</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama"
                                       value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Username:</label>
                                <input type="text" name="username" class="form-control" placeholder="Username"
                                       value="{{old('username',(isset($detail)? $detail->username : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" name="password" id="password" class="form-control"
                                       placeholder="{{(isset($detail->id)? 'Kosongi jika tidak mengganti password.' : 'Password')}}"
                                    {{(isset($detail->id)? '' : 'required')}}
                                >
                            </div>
                            <div class="form-group">
                                <label>Validasi Password:</label>
                                <input type="password" name="validasi_password" id="validasi_password"
                                       class="form-control"
                                       placeholder="{{(isset($detail->id)? 'Kosongi jika tidak mengganti password.' : 'Validasi Password')}}"
                                       data-parsley-equalto="#password"
                                    {{(isset($detail->id)? '' : 'required')}}
                                >
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <div class="input-group">
                                    <select class="form-control kt-select2"
                                            name="roles_id"
                                            required>
                                        <option></option>
                                        @foreach ($role as $r)
                                            <option
                                                value="{{$r->id}}" {{(old('roles_id', isset($detail->roles_id) ? $detail->roles_id : '' ) == $r->id ? 'selected':'') }}>{{$r->nama_role}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--border-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Akses Cabang:</p>
                                    <div id="gridContainer"></div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                thisForm.dataGrid();
                $('#form').submit(function (e) {
                    // e.preventDefault();
                    $data_access = $access.selectedKeyNames();
                    $data_access = JSON.stringify($data_access);

                    $('<input>', {
                        type: 'hidden',
                        name: 'users_company',
                        value: $data_access
                    }).appendTo($(this));

                    return true;
                });
            },
            dataGrid: function () {
                $access = $("#gridContainer").kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                data: {id: '{{(isset($detail)? $detail->id : 0)}}'},
                                url: "{{url()->current()}}",
                                dataType: "jsonp",
                            }
                        },
                        schema: {
                            model: {
                                id: "id"
                            }
                        },
                    },
                    selectable: "multiple",
                    filterable: true,
                    columnMenu: true,
                    sortable: true,
                    resizable: true,
                    reorderable: true,
                    groupable: false,
                    persistSelection: true,
                    dataBound: onDataBound,
                    pageable: false,
                    columns: [
                        {selectable: true, width: "50px"},
                        {field: "cabang", title: "Cabang"},
                        {field: "nama", title: "Nama"},
                    ]
                }).data("kendoGrid");
            }
        };

        function onDataBound(e) {
            const grid = this;
            const rows = grid.items();
            $(rows).each(function (e) {
                const row = this;
                const dataItem = grid.dataItem(row);
                if (dataItem.granted > 0) {
                    grid.select(row);
                }
            });
        }

    </script>
@endpush
