@extends('admin.layouts.default')
@section('title', $title='List Invoice')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                        {{--                        <div class="kt-portlet__head-toolbar">--}}
                        {{--                            <div class="kt-portlet__head-wrapper">--}}
                        {{--                                <div class="kt-portlet__head-actions">--}}
                        {{--                                    <a href="{{url('invoice/data_trip/detail')}}"--}}
                        {{--                                       class="btn btn-success btn-elevate btn-icon-sm">--}}
                        {{--                                        <i class="la la-plus"></i>--}}
                        {{--                                        Buat Baru--}}
                        {{--                                    </a>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="kt-portlet__body">
                        <table class="table table-striped- table-bordered table-hover" id="table-dt">
                            <thead>
                            <tr>
                                <th>No. Invoice</th>
                                <th>No. Trip</th>
                                <th class="currency">Tagihan</th>
                                <th>Penyewa</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                                <th>Checker</th>
                                <th>Pengirim</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                $dt = $('#table-dt').DataTable({
                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                    ],
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: "{{url()->current()}}",
                    createdRow: defForm.dt_createdRow,
                    columns: [
                        {data: 'no_invoice', name: 'no_invoice'},
                        {data: 'no_trip', name: 'no_trip'},
                        {data: 'total_tagihan', name: 'total_tagihan'},
                        {data: 'penyewa', name: 'penyewa'},
                        {data: 'tanggal', name: 'tanggal'},
                        {data: 'keterangan', name: 'keterangan'},
                        {data: 'status', name: 'status'},
                        {data: 'checker', name: 'checker'},
                        {data: 'users.username', name: 'users.username'},
                        {data: 'action', name: 'action'},
                    ],
                    drawCallback: function (settings) {
                        defForm.init();
                    },
                    lengthMenu: [[50, -1], [50, "All"]],
                });
            },
        }
    </script>
@endpush
