@extends('admin.layouts.default')
@section('title', $title='Approve_print')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <form id="form_validation" method="post" action="{{ route('deskripsi.post')}}"
                      novalidate="novalidate">
                    @csrf
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    {{$title}}
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-wrapper">
                                    <div class="kt-portlet__head-actions">
                                        <button type="submit" class="btn btn-primary btn-elevate btn-icon-sm">UPDATE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover" id="table-dt">
                                <thead>
                                <tr>
                                    <th>No. Trip</th>
                                    <th>Tanggal</th>
                                    <th>Total</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($data as $d)
                                    <tr>
                                        <td>{{$d->no_trip}}</td>
                                        <td>{{$d->tanggal}}</td>
                                        <td>{{$d->total_tagihan}}</td>
                                        <td><textarea type="text"
                                                      onkeyup="this.value = this.value.toUpperCase();"
                                                      class="form-control" name="invoice[{{$d->id}}][description]"
                                                      placeholder="Masukkan Deskripsi">{{$d->deskripsi}}</textarea>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
{{--    <script>--}}
{{--        $(function () {--}}
{{--            thisForm.init();--}}
{{--        }), thisForm = {--}}
{{--            init: function () {--}}
{{--                $dt = $('#table-dt').DataTable({--}}
{{--                    dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,--}}
{{--                    buttons: [--}}
{{--                        'copyHtml5',--}}
{{--                        'excelHtml5',--}}
{{--                    ],--}}
{{--                });--}}
{{--            },--}}
{{--        }--}}
{{--    </script>--}}
@endpush
