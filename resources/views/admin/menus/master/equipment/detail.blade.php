@extends('admin.layouts.default')
@section('title', $title='Equipment Expenses')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('admin.includes.alert')
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{route('equipment_expenses.log')}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @foreach ($equipment as $e)
                                <input type="hidden" class="form-control" name="header_id"
                                       value="{{$e->id}}">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Model Serie:</label>
                                            <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                                   class="form-control"
                                                   placeholder="Purchase No"
                                                   value="{{$e->model_series}}"
                                                   disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal:</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control date-picker"
                                                       name="tanggal"
                                                       value="{{old('trans_date',(isset($detail)? $detail->tanggal : ''))}}"
                                                       placeholder="Select date"
                                                    {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                />
                                                <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Jenis Biaya</label>
                                            <div class="input-group">
                                                <select class="form-control kt-select2"
                                                        name="jenis_biaya"
                                                        required>
                                                    <option value="SPARE PART">SPARE PART</option>
                                                    <option value="SERVICE">SERVICE</option>
                                                    <option value="LAIN-LAIN">LAIN - LAIN</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Jumlah:</label>
                                            <input type="text"
                                                   name="jumlah"
                                                   class="form-control price input-currency" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Supplier:</label>
                                            <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                                   name="supplier" class="form-control"
                                                   placeholder="Supplier">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Keterangan:</label>
                                            <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                                   name="keterangan"
                                                   class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12 ml-lg-auto">
                                                <button type="submit" class="btn btn-brand">Submit</button>
                                                <a data-url="{{url()->previous()}}"
                                                   class="btn btn-secondary prevent-dialog"
                                                   data-sw-title="Yakin Cancel?">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover" id="table-dt">
                                <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Jenis Biaya</th>
                                    <th>Jumlah</th>
                                    <th>Supplier</th>
                                    <th>Keterangan</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{ date('d-m-Y',strtotime($d->tanggal)) }}</td>
                                        <td>{{$d->jenis_biaya}}</td>
                                        <td> Rp {{ number_format($d->jumlah,'0', '.', ',') }}</td>
                                        <td>{{$d->supplier}}</td>
                                        <td>{{$d->keterangan}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
