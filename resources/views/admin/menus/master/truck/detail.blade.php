@extends('admin.layouts.default')
@section('title', $title='Truck')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Nopol:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="no_pol" class="form-control" placeholder="Nopol"
                                       value="{{old('no_pol',(isset($detail)? $detail->no_pol : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Jenis Truck:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="jenis" class="form-control" placeholder="Jenis"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Type</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="type" class="form-control" placeholder="Type"
                                       value="{{old('type',(isset($detail)? $detail->type : ''))}}"
                                       required>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
