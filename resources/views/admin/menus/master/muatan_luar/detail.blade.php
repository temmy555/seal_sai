@extends('admin.layouts.default')
@section('title', $title='Muatan Luar')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Penerima:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="penerima" class="form-control" placeholder="Penerima"
                                       value="{{old('penerima',(isset($detail)? $detail->penerima : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Jenis Biaya:</label>
                                <input type="text" name="jenis_biaya" class="form-control" placeholder="Jenis Biaya"
                                       value="MUATAN LUAR"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label>Tanggal:</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control date-picker"
                                           name="tanggal"
                                           value="{{old('tanggal',(isset($detail)? $detail->tanggal : ''))}}"
                                           placeholder="Select date"
                                        {{(isset($detail)? 'disabled="disabled"' : '')}}
                                    />
                                    <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Penyewa:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="penyewa" class="form-control" placeholder="Penyewa"
                                       value="{{old('penyewa',(isset($detail)? $detail->penyewa : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Muatan:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="muatan" class="form-control" placeholder="Muatan"
                                       value="{{old('muatan',(isset($detail)? $detail->muatan : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Nopol:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="no_pol" class="form-control" placeholder="Nopol"
                                       value="{{old('no_pol',(isset($detail)? $detail->no_pol : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Nilai Sangu:</label>
                                <input type="text"
                                       name="jumlah"
                                       class="form-control price input-currency" required>
                            </div>
                            <div class="form-group">
                                <label>Tagihan Awal:</label>
                                <input type="text"
                                       name="nilai_awal"
                                       class="form-control price input-currency" required>
                            </div>
                            <div class="form-group">
                                <label>Awal:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="asal" class="form-control" placeholder="Awal"
                                       value="{{old('asal',(isset($detail)? $detail->asal : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Tujuan:</label>
                                <input type="text" onkeyup="this.value = this.value.toUpperCase();" name="tujuan" class="form-control" placeholder="Tujuan"
                                       value="{{old('tujuan',(isset($detail)? $detail->tujuan : ''))}}"
                                       required>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
