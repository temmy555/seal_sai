@extends('admin.layouts.default')
@section('title', $title='Uang kerja')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="purchase_header_id" value="{{$detail->purchase_header_id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Model Serie</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    name="jenis_biaya"
                                                    required>
                                                <option value="SPARE PART">KOMATSU CRANE X-101
                                                    100M
                                                </option>
                                                <option value="SERVICE">HYNO TRUCK</option>
                                                <option value="LAIN-LAIN">LAIN - LAIN</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Penyewa:</label>
                                        <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                               name="purchase_no" class="form-control"
                                               placeholder="Penyewa">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Tanggal:</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control date-picker"
                                                   name="trans_date"
                                                   value="{{old('trans_date',(isset($detail)? $detail->trans_date : ''))}}"
                                                   placeholder="Select date"
                                                {{(isset($detail)? 'disabled="disabled"' : '')}}
                                            />
                                            <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table" id="dynamicTable">
                                        <tr>
                                            <td><input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                                       name="addmore[0][penerima]" class="form-control"
                                                       placeholder="Penerima"></td>
                                            <td><input type="number" onkeyup="this.value = this.value.toUpperCase();"
                                                       name="addmore[0][jumlah]" class="form-control"
                                                       placeholder="jumlah"></td>
                                            <td><input type="text" name="addmore[0][keterangan]"
                                                       placeholder="Keterangan Periode"
                                                       class="form-control"/></td>
                                            <td>
                                                <button type="button" name="add[]" id="add"
                                                        class="btn btn-bold btn-sm btn-label-brand"><i
                                                        class="la la-plus">Add</i>
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <table class="table table-striped- table-bordered table-hover" id="table-dt">
                                <thead>
                                <tr>
                                    <th width="50">Select</th>
                                    <th>No Trip</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Biaya</th>
                                    <th>Penyewa</th>
                                    <th>Penerima</th>
                                    <th>Jumlah</th>
                                    <th>Keterangan Periode</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox" name="check[]" class="form-control"></td>
                                    <td>UK1</td>
                                    <td>06/06/2020</td>
                                    <td>Uang Kerja</td>
                                    <td>Budi</td>
                                    <td>Hartono</td>
                                    <td>1,500,000</td>
                                    <td>12 Jam</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        var i = 0;
        $("#add").click(function () {
            ++i;
            $("#dynamicTable").append('<tr><td><input type="text" name="addmore[' + i + '][penerima]" placeholder="Penerima" class="form-control" /></td><td><input type="number" name="addmore[' + i + '][jumlah]" placeholder="Jumlah" class="form-control" /></td><td><input type="text" name="addmore[' + i + '][Keterangan]" placeholder="Keterangan Periode" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-sm remove-tr"><i\n' +
                '                                                                class="la la-trash">Remove</i></button></td></tr>');
        });

        $(document).on('click', '.remove-tr', function () {
            $(this).parents('tr').remove();
        });

    </script>
@endpush
