<form class="kt-form kt-form--label-right form-validatejs form-filter" method="get"
      action="{{url()->current()}}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Awal</label>
                <div class="input-group date">
                    <input name="filter_awal_date" type="text"
                           class="form-control date-picker"
                           placeholder="Select date"/>
                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Tanggal Akhir</label>
                <div class="input-group date">
                    <input name="filter_akhir_date" type="text"
                           class="form-control date-picker"
                           placeholder="Select date"/>
                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-form__actions">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-success">
                    Filter
                    <i class="la la-filter"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="kt-separator kt-separator--fit"></div>
</form>
