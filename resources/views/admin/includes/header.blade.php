<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default"></div>
    </div>
    <div class="kt-header__topbar">
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                <div class="kt-header__topbar-user">
                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                    <span class="kt-header__topbar-username kt-hidden-mobile">{{ucfirst(auth()->user()->nama)}}</span>
                    {{--                    <img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg"/>--}}
                    <span
                        class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{Auth::user()->inisial}}</span>
                </div>
            </div>
            <div
                class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                <div class="kt-notification">
                    @if(auth()->user()->count_company_id > 1)
                        <a href="javascript:void(0);" id="change_cabang"
                           class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="flaticon-home-2 kt-font-primary"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Pindah Cabang
                                </div>
                            </div>
                        </a>
                    @endif
                    <a href="{{route('password.reset')}}"
                       class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-warning kt-font-warning"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                Ganti Password
                            </div>
                        </div>
                    </a>
                    <a href="{{url('logout')}}"
                       class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-logout kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                Logout
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        $('#change_cabang').on('click', function () {
            onChangeCabangClick();
        })
    </script>
@endpush
