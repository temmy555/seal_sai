<meta name="description" content="Form controls validation">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
{{--<link href="{{ asset('assets/media/logos/favicon.ico') }}" rel="shortcut icon"/>--}}
<style>
    tr.dtrg-group {
        background: #d3d3d3;
    }

    .sidebar-logo {
        width: 150px;
        height: 40px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 1.5 !important;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #e2e5ec !important;
        border-radius: 4px !important;
    }

    .select2-container .select2-selection--single {
        height: auto !important;
    }

    .select2-container *:focus {
        box-shadow: none !important;
        color: #495057 !important;
        background-color: #fff !important;
        border-color: #9aabff !important;
        outline: 0 !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 50% !important;
    }

    .select2-selection__clear {
        top: 43% !important;
    }

    select.form-control {
        border-radius: 25px !important;
    }

    input.form-control, select.form-control {
        color: black;
        font-weight: 300;
    }

    input:focus, select:focus {
        color: black !important;
        font-weight: 400 !important;
    }

    input[readonly], input[disabled], select[disabled] {
        background-color: #eee !important;
    }

    span.select2-selection__rendered {
        color: black !important;
        font-weight: 300;
    }

    .is-invalid .select2-container--default .select2-selection--multiple, .is-invalid .select2-container--default .select2-selection--single {
        border-color: #fd397a !important;
    }

    .is-invalid .select2-container--default .select2-selection--multiple, .is-valid .select2-container--default .select2-selection--single {
        border-color: #0abb87 !important;
    }

    div.k-grid th {
        color: black !important;
    }
</style>
