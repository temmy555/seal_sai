<div id="modal_cabang" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sarinah Group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="kt-form kt-form--label-right form-validatejs" method="post" id="popup_cabang"
                  action="{{route('tools.user_cabang')}}">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Pilih Cabang</label>
                        <div class="input-group">
                            <select class="form-control kt-select2"
                                    name="active_company_id" id="active_company_id"
                                    required>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('script')
    <script>
        function onChangeCabangClick() {
            $.ajax({
                type: "GET",
                url: "{{route('tools.user_cabang')}}",
                success: function ($res) {
                    if (!$res.length) {
                        alert('you dont have access');
                        return;
                    }
                    $('#active_company_id').empty();
                    $.each($res, function (i, v) {
                        $('<option>').val(v.id).text("(" + v.cabang + ") " + v.nama).appendTo('#active_company_id');
                    });
                    if($res.length == 1){
                        $('#popup_cabang').submit();
                        return;
                    }
                    $('#modal_cabang').modal('show');
                },
            });
        };
    </script>
@endpush
